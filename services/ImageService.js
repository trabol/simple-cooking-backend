var pathConfig = require("../config/paths");
const fs = require("nano-fs");

class ImageService{
  constructor(path){
    this.path=pathConfig.local.images;
    this.httpPath=pathConfig.http.images;
    console.log(this.path);
  }
  async saveImage(name,data,options){//saves tmp images on server which will be later collected and saved on image server by background process
    
    await fs.mkpath(this.path);
    await fs.writeFile(this.path+name,data,"base64");
    console.log("image saved to "+this.path+name);
    return this.httpPath+name;
  }
}

var service = new ImageService("/public/images");
module.exports = service;
