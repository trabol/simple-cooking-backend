var User = function(data){
  this.data=data;
}

User.prototype.toResponse=function(isPublic){
  if(isPublic){
  return{
    id:this.data.id,
    name:this.data.name,
    email:this.data.email,
    picture:this.data.picture

  }
  }
  var response=this.data;
  return response;
}


module.exports= User;
