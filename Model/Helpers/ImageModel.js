
class ImageModel{
  constructor(data){
    console.log("data");
    console.log(data);
    this.id=data.id||null;
    this.location=data.location||"";
    this.authorId=data.authorId||null;
  }
  toResponse(){
    return{
      id:this.id,
      location:this.location,
      authorId:this.authorId
    }
  }
}

module.exports=ImageModel;
