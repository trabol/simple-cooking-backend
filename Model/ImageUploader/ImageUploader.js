const multer = require('multer');
const sharp = require("sharp");
var pathConfig = require("../../config/paths");
const Error = require("../../Utils/Error");
const util = require("util");
const path = require("path");
class ImageUploader{
  constructor(){
    this.path=pathConfig.local.images;
    this.httpPath=pathConfig.http.images;
  }
  async upload(req,res,fileFormName,name){
    try{
      console.log(name);
      var self = this;
      var storageLocation = multer.diskStorage({
        destination:function(req,res,next){
          next(null,self.path);
        },
        filename: function(req,res,next){
          console.log(name);
            next(null,name);
        }
      });
      var multerUp = multer({
        storage:storageLocation,
        limits: { fileSize: 1024*20 }
        fileFilter: function (req, file, cb) {
          console.log(file);
          if (path.extname(file.originalname) !== '.png') {
            cb(Error.Errors.WRONG_FILE_FORMAT)
          }
          console.log("endFilter");
          cb(null,req,file);
        }
      });

    var upload = util.promisify(multerUp.single(fileFormName));
    console.log("upload prepared");
    await upload(req,res);
    console.log("uploaded");
  }catch(err){
    console.log("err");
    console.log(err);
    if(err.status&&err.code)throw err;
    else throw Error.Errors.INTERNAL;
  }
  return this.httpPath+name;
  }
}

module.exports=new ImageUploader();
