const MySQLWrapper= require('./MySQLWrapper/MySQL')
let mysql=new MySQLWrapper();
const async = require('async');
const dbConfig=require('../../config/db');
const Error= require('../../Utils/Error');

let _pool= new WeakMap();
class ConnectionManager{
  constructor(){
    _pool.set(this, mysql.createPool(dbConfig.credentials));
  }
  async getConnection(){
    let pool=_pool.get(this);
    console.log(pool.pool._freeConnections.length);
    console.log("I want connection");
    let connection=await pool.getConnection();
    console.log("I have connection");
    return connection;
  }
}

  let connectionManager=new ConnectionManager();

  module.exports=connectionManager;
