let DBHandler =require( "./DBHandler");
let Contract =require( "./DBContract");
let authConfig=require("../../config/auth");
let Pair = require("../../Utils/Pair");
let Error= require('../../Utils/Error');
let User = require("../Helpers/UserModel");
let Ingredient = require("../Helpers/IngredientModel");
let QueryBuilder= require('./QueryBuilder');


class IngredientDBHandler extends DBHandler{
  constructor(connection,parent){
    super(connection,parent);
  }
  async getById(id){
    let rows = await this.connection.query("SELECT "+Contract.Ingredient.COLUMN_NAME+" FROM "+Contract.Ingredient.TABLE_NAME+" WHERE "+Contract.Ingredient.COLUMN_ID+"=?",id,);
    if(rows.length){
      return new Ingredient({id:id,name:rows[0][Contract.Ingredient.COLUMN_NAME]});
    }else{
      throw Error.Errors.INGREDIENT_DOESNT_EXIST;
    }
  }
  async getByName(name){
    let rows = await this.connection.query("SELECT "+Contract.Ingredient.COLUMN_ID+" FROM "+ Contract.Ingredient.TABLE_NAME+" WHERE "+ Contract.Ingredient.COLUMN_NAME+"=?",name);
    if(rows.length){
      return new Ingredient({
        id:rows[0]["ingredient_id"],
        name:name
      });
    }else{
      throw Error.Errors.INGREDIENT_DOESNT_EXIST;
    }
  }
  async getIngredientsByName(ingredients){
    let params=[];
    for(let i=0;i<ingredients.length;i++){
      params.push(ingredients[i].data.name);
    }
    let query=new QueryBuilder()
      .select([Contract.Ingredient.COLUMN_ID,Contract.Ingredient.COLUMN_NAME])
      .from(Contract.Ingredient.TABLE_NAME)
      .where(Contract.Ingredient.COLUMN_NAME)
      .inQue(params).build();
      console.log(query);
    let rows=await this.connection.query(query,params);
    console.log("queried");
    if(!rows.length||rows.length<params.length)throw Error.Errors.INGREDIENT_DOESNT_EXIST;
    console.log(rows);
    return rowsToIngredients(rows);
  }//TODO efficient call
  async save(ingredient){
    let rows = await this.connection.query("INSERT INTO "+Contract.Ingredient.TABLE_NAME+"("+Contract.Ingredient.COLUMN_NAME+")VALUES(?)",ingredient.data.name);
    ingredient.data.id=rows.insertId;
    return ingredient;
  }
  async update(ingredient){
    console.log("UPDATE "+Contract.Ingredient.TABLE_NAME+" SET "+Contract.Ingredient.COLUMN_NAME+"=?"+" WHERE "+Contract.Ingredient.COLUMN_ID+"=?");
    let rows = await this.connection.query("UPDATE "+Contract.Ingredient.TABLE_NAME+" SET "+Contract.Ingredient.COLUMN_NAME+"=?"+" WHERE "+Contract.Ingredient.COLUMN_ID+"=?",[ingredient.data.name,ingredient.data.id]);
  }
  async deleteIt(ingredient){
    let rows = await  this.connection.query("SELECT "+Contract.IngredientToDish.COLUMN_DISH_ID+" FROM "+Contract.IngredientToDish.TABLE_NAME+" WHERE "+ Contract.IngredientToDish.COLUMN_INGREDIENT_ID+"=?",ingredient.data.id);
    if(rows.length){
      var query1="DELETE FROM ingredient_to_dish WHERE ";
      var params1 =[];
      for(var i=0;i<result.length;i++){
        query1+="dish_id= ?"
        params1.push(result[i]["dish_id"]);
        if(i!=result.length-1)query1+=" OR ";
      }

      var query2="DELETE FROM dishes WHERE ";
      var params2 =[];
      for(var i=0;i<result.length;i++){
        query2+="dish_id= ?"
        params2.push(result[i]["dish_id"]);
        if(i!=result.length-1)query2+=" OR ";
      }
      let calls=[];
      if(params1.length)cals.push(this.connection.query(query1,params1));
      if(params2.length)cals.push(this.connection.query(query2,params2));
      let results=await Promise.all(calls);
    }
    let final = await this.connection.query("DELETE FROM "+Contract.Ingredient.TABLE_NAME+" WHERE "+Contract.Ingredient.COLUMN_ID+"=?",ingredient.data.id);
    return final;
  }
}

function rowToIngredient(row){
  console.log("row");
  console.log(row);
  return new Ingredient({
    id:row[Contract.Ingredient.COLUMN_ID],
    name:row[Contract.Ingredient.COLUMN_NAME]
  });
}

function rowsToIngredients(rows){
  let array=[]
  for(let i=0;i<rows.length;i++){
    let ingredient=rowToIngredient[rows[i]];
    array.push(new Ingredient({
      id:rows[i][Contract.Ingredient.COLUMN_ID],
      name:rows[i][Contract.Ingredient.COLUMN_NAME]
    }));

  }
  return array;
}


module.exports=IngredientDBHandler;
