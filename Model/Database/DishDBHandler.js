let DBHandler =require( "./DBHandler");
let Contract =require( "./DBContract");
let async = require('async');
let googleStuff = require('../../services/googleVB');
let crypto = require('crypto');
let jwt=require("jsonwebtoken");
let authConfig=require("../../config/auth");
let Pair = require("../../Utils/Pair");
let Error= require('../../Utils/Error');
let User = require("../Helpers/UserModel");
let Dish = require("../Helpers/DishModel");
let Ingredient = require("../Helpers/IngredientModel");
let QueryBuilder = require('./QueryBuilder');

class DishDBHandler extends DBHandler{
  constructor(connection,parent){
    super(connection,parent);
  }
  async getById(id) {
    console.log("ID");
    console.log(id);
    let rows = await this.connection.query("SELECT "+Contract.Dish.COLUMN_NAME+","+Contract.Dish.COLUMN_RECIPE+","+Contract.Dish.COLUMN_AUTHOR_ID+" from "+Contract.Dish.TABLE_NAME+" where "+Contract.Dish.COLUMN_ID+"=?",id);
    if(!rows.length)throw Error.Errors.DISH_DOESNT_EXIST;
    let dish = new Dish({
      id:id,
      name:rows[0].name,
      recipe:rows[0].recipe,
      authorId:rows[0].author_id
    });
    dish.data.ingredients=await this.getDishIngredients(dish);
    return dish;
  }

  async getDishIngredients(dish){
    let rows = await this.connection.query(new QueryBuilder().selectB(Contract.Ingredient.TABLE_NAME,[Contract.Ingredient.COLUMN_NAME,Contract.Ingredient.COLUMN_ID])
      .from(Contract.Ingredient.TABLE_NAME)
      .innerJoin(Contract.IngredientToDish.TABLE_NAME)
      .on(Contract.Ingredient.TABLE_NAME,Contract.Ingredient.COLUMN_ID,Contract.IngredientToDish.TABLE_NAME,Contract.IngredientToDish.COLUMN_INGREDIENT_ID)
      .where(Contract.Dish.COLUMN_ID+"=?").build(),dish.data.id);
    if(!rows.length)throw Error.Errors.INGREDIENT_DOESNT_EXIST;
    let ingredients=[];
    for(let i=0;i<rows.length;i++){
      ingredients.push(new Ingredient({
        name:rows[i].ingredient_name,
        id:rows[i].ingredient_id}));
    }
    return ingredients;
  }
  async getByIngredients(ingredients){
    let inputIds=[];
    for(let i=0;i<ingredients.length;i++){
      inputIds.push(ingredients[i].data.id);
    }
    let query= new QueryBuilder().select([Contract.Dish.TABLE_NAME+"."+Contract.Dish.COLUMN_ID,Contract.Dish.TABLE_NAME+"."+Contract.Dish.COLUMN_NAME,Contract.Dish.TABLE_NAME+"."+Contract.Dish.COLUMN_RECIPE,Contract.Dish.TABLE_NAME+"."+Contract.Dish.COLUMN_AUTHOR_ID])
      .from(Contract.Dish.TABLE_NAME)
      .leftOuterJoin(Contract.IngredientToDish.TABLE_NAME)
        .on(Contract.Dish.TABLE_NAME,Contract.Dish.COLUMN_ID,Contract.IngredientToDish.TABLE_NAME,Contract.IngredientToDish.COLUMN_DISH_ID)
        .and("").notInQueT(Contract.IngredientToDish.TABLE_NAME,Contract.IngredientToDish.COLUMN_INGREDIENT_ID,inputIds.length)
        .where("").isNull(Contract.IngredientToDish.TABLE_NAME,Contract.IngredientToDish.COLUMN_DISH_ID);
    let rows = await this.connection.query(query.build(),inputIds);
    if(!rows.length)throw Error.Errors.NO_DISH_WITH_INGREDIENTS;
    let dishes = this.rowsToDishes(rows);
    return dishes;
  }//TODO optymalize query


  async checkIfExists(dish){
    let rows = await this.connection.query(new QueryBuilder().select(Contract.Dish.COLUMN_ID)
      .from(Contract.Dish.TABLE_NAME)
      .where(Contract.Dish.COLUMN_NAME+"=?").build(),dish.data.name);
    if(!rows.length)return false;
    return rows[0][Contract.Dish.COLUMN_ID];
  }
  async save(dish){//TODO test
    let rows = await  this.connection.query(new QueryBuilder()
      .insertInto(Contract.Dish.TABLE_NAME,[Contract.Dish.COLUMN_NAME,Contract.Dish.COLUMN_RECIPE,Contract.Dish.COLUMN_AUTHOR_ID])
      .values(["?","?","?"]).build(),[dish.data.name,dish.data.recipe,dish.data.authorId]);
    dish.data.id=rows.insertId;
    let params=[];
    for(let i=0;i<dish.data.ingredients.length;i++){
      params.push(dish.data.id);
      params.push(dish.data.ingredients[i].data.id);
    }
    //TODO insert many ingredients connections
    rows = await this.connection.query(new QueryBuilder().insertInto(Contract.IngredientToDish.TABLE_NAME,[Contract.IngredientToDish.COLUMN_DISH_ID,Contract.IngredientToDish.COLUMN_INGREDIENT_ID])
      .manyValuesQue(params.length/2,2).build(),params);
    return dish;
  }
  async delete(dish){
    await this.connection.query(new QueryBuilder().Delete()
      .from(Contract.Dish.TABLE_NAME)
      .where(Contract.Dish.COLUMN_ID+"=?")
      .build(),dish.data.id);
    await this.connection.query(new QueryBuilder().Delete()
      .from(Contract.IngredientToDish.TABLE_NAME)
      .where(Contract.IngredientToDish.COLUMN_DISH_ID+"=?")
      .build("a"),dish.data.id);
  }
  async updateBasic(dish){
    console.log("data");
    console.log(new QueryBuilder().update(Contract.Dish.TABLE_NAME)
      .set([Contract.Dish.COLUMN_NAME,Contract.Dish.COLUMN_RECIPE/*,Contract.Dish.COLUMN_PICURE*/])
      .where().build());
      console.log(Contract.Dish.COLUMN_ID);
    console.log([dish.data.name,dish.data.recipe,dish.data.id]);
    let rows = await this.connection.query(new QueryBuilder().update(Contract.Dish.TABLE_NAME)
      .set([Contract.Dish.COLUMN_NAME,Contract.Dish.COLUMN_RECIPE/*,Contract.Dish.COLUMN_PICURE*/])
      .where(Contract.Dish.COLUMN_ID+"=?").build(),[dish.data.name,dish.data.recipe/*,dish.data.picture*/,dish.data.id]);
    return rows;
  }

  async updateAll(oldDish,newDish){
    if(!oldDish)oldDish=await this.getById(newDish.data.id);
    let rows = await this.updateBasic(newDish);
    let similar = similarElements(newDish.data.ingredients,oldDish.data.ingredients);
    let toAdd=differentElements(similar,newDish.data.ingredients);
    let toDelete=differentElements(similar,oldDish.data.ingredients);
    let query1="DELETE FROM ingredient_to_dish WHERE dish_id=? AND (";
    let params1=[];
    params1.push(oldDish.data.id);
    for(let i=0;i<toDelete.length;i++){//TODO queryMaker
      query1+="ingredient_id=? ";
      params1.push(toDelete[i].data.id);
      if(i!=toDelete.length-1)query1+="OR";
    }
    query1+=")";
    let query2="INSERT INTO ingredient_to_dish(ingredient_id,dish_id) VALUES";
    let params2=[];
    for(let i=0;i<toAdd.length;i++){//TODO queryMaker
      query2+="(?,?) ";
      params2.push(toAdd[i].data.id);
      params2.push(oldDish.data.id);
      if(i!=toAdd.length-1)query2+=",";
    }
    let calls=[];
    console.log("connection");
    console.log(query1);
    console.log(params1);
    console.log(query2);
    console.log(params2);
    if(params1.length>1)calls.push(this.connection.query(query1,params1));
    if(params2.length)calls.push(this.connection.query(query2,params2));
    console.log(calls);
    let results = await Promise.all(calls);
    return results;
  }

  async getByAuthorId(id){
    console.log(new QueryBuilder().select([Contract.Dish.COLUMN_ID,Contract.Dish.COLUMN_NAME,Contract.Dish.COLUMN_RECIPE,Contract.Dish.COLUMN_AUTHOR_ID])
    .where(Contract.Dish.COLUMN_AUTHOR_ID+"=?").build());
    let rows = await this.connection.query(new QueryBuilder().select([Contract.Dish.COLUMN_ID,Contract.Dish.COLUMN_NAME,Contract.Dish.COLUMN_RECIPE,Contract.Dish.COLUMN_AUTHOR_ID])
    .from(Contract.Dish.TABLE_NAME)
    .where(Contract.Dish.COLUMN_AUTHOR_ID+"=?").build(),id);
    console.log("qwerty");
    let size=rows.length;
    let dishes=[];
    for(let i=0;i<size;i++){
      let dish = this.rowToDish(rows[i]);
      dishes.push(dish);
    }
    console.log(dishes);
    let ingredientsWithKey = await this.getIngredientsForManyDishes(dishes);
    let dishesMap=new Map();
    for(let i=0;i<dishes.length;i++){
      dishesMap.set(dishes[i].data.id,dishes[i]);
    }
    for(let i=0;i<ingredientsWithKey.length;i++){
      dishesMap.get(ingredientsWithKey[i][0]).data.ingredients.push(ingredientsWithKey[i][1]);
    }
    console.log(dishes);
    return dishes;
  }
  //TODO models from rows to object converters
  async getIngredientsForManyDishes(dishes){//format dishes[i].data.id=i;
    console.log(dishes);
    let whereClause="";
    let whereParams=[];
    for(let i=0;i<dishes.length;i++){
      whereClause+=Contract.Dish.TABLE_NAME+"."+Contract.Dish.COLUMN_ID+"=?";
      whereParams.push(dishes[i].data.id);
      if(i<dishes.length-1)whereClause+=" OR ";
    }
    let rows = await this.connection.query(new QueryBuilder().select([Contract.IngredientToDish.TABLE_NAME+"."+Contract.IngredientToDish.COLUMN_DISH_ID,
        Contract.Ingredient.TABLE_NAME+"."+Contract.Ingredient.COLUMN_ID,
        Contract.Ingredient.TABLE_NAME+"."+Contract.Ingredient.COLUMN_NAME])
      .from(Contract.IngredientToDish.TABLE_NAME)
      .innerJoin(Contract.Ingredient.TABLE_NAME)
        .on(Contract.IngredientToDish.TABLE_NAME,Contract.IngredientToDish.COLUMN_INGREDIENT_ID,Contract.Ingredient.TABLE_NAME,Contract.Ingredient.COLUMN_ID)
      .innerJoin(Contract.Dish.TABLE_NAME)
        .on(Contract.IngredientToDish.TABLE_NAME,Contract.IngredientToDish.COLUMN_DISH_ID,Contract.Dish.TABLE_NAME,Contract.Dish.COLUMN_ID)
      .where(whereClause).build(),whereParams);
      let ingredientsWithKey=[];
      for(let i=0;i<rows.length;i++){//TODO move binding away and use map
        ingredientsWithKey.push([rows[i][Contract.IngredientToDish.COLUMN_DISH_ID],
        this.rowToIngredient(rows[i])]);
      }
      return ingredientsWithKey;
    }
    rowToIngredient(row){
      return new Ingredient({
        id:row[Contract.Ingredient.COLUMN_ID],
        name:row[Contract.Ingredient.COLUMN_NAME]
      });
    }

  rowToDish(row){
    return new Dish({
      id:row[Contract.Dish.COLUMN_ID],
      name:row[Contract.Dish.COLUMN_NAME],
      recipe:row[Contract.Dish.COLUMN_RECIPE],
      authorId:row[Contract.Dish.COLUMN_AUTHOR_ID],
      ingredients:[]
    });
  }

  rowsToDishes(rows){
    let size=rows.length;
    let dishes=[];
    for(let i=0;i<size;i++){
      dishes.push(this.rowToDish(rows[i]));
    }
    return dishes;
  }
}


function similarElements(array1,array2){
  var lookupArray=[];

  var similar=[];
  for(let i=0;i<array1.length;i++){
    lookupArray[array1[i].data.name]=1;
  }
  for(let i=0;i<array2.length;i++){
    if(lookupArray[array2[i].data.name]){
      similar.push(array2[i]);
    }
  }
  return similar;
}
function differentElements(array1,array2){
  var lookupArray=[];

  var different=[];
  for(let i=0;i<array1.length;i++){
    lookupArray[array1[i].data.name]=1;
  }
  for(let i=0;i<array2.length;i++){
    if(!lookupArray[array2[i].data.name]){
      different.push(array2[i]);
    }
  }
  return different;
}

function getCombinations(input){
  var results=[];
    var f=function(it,depth,size,length,array){
    if(depth>=size)return;
    if(depth==size-1){
      for(let i=it;i<length;i++){
        array.push(input[i]);
        results.push(array.slice());
        array.pop();
      }
    }else{
      for(let i=it;i<length;i++){
        console.log("pushing "+input[i]);
        array.push(input[i]);
        f(i+1,depth+1,size,length,array);
        array.pop();
      }
    }
  }
  for(let i=1;i<=input.length;i++){
    f(0,0,i,input.length,[]);
  }
  return results;
}
module.exports=DishDBHandler;
