let DBHandler =require( "./DBHandler");
let Contract =require( "./DBContract");
let authConfig=require("../../config/auth");
let Pair = require("../../Utils/Pair");
let Error= require('../../Utils/Error');
let User = require("../Helpers/UserModel");
let Ingredient = require("../Helpers/IngredientModel");
let Image = require("../Helpers/ImageModel");
let QueryBuilder= require('./QueryBuilder');



class ImageDBHandler extends DBHandler{
  constructor(connection,parent){
    super(connection,parent);
  }
  async getById(id){

    let query =new QueryBuilder().select([Contract.Image.COLUMN_LOCATION,Contract.Image.COLUMN_AUTHOR_ID])
    .from(Contract.Image.TABLE_NAME)
    .where(Contract.Image.COLUMN_ID+"=?").build();
    let rows = await this.connection.query(query,id);
    if(rows.length){
      rows[Contract.Image.COLUMN_ID]=id;
      return rowsToImage(rows);
    }else{
      throw Error.Errors.INGREDIENT_DOESNT_EXIST;
    }
  }

  async create(authorId){

    let query = new QueryBuilder().insertInto(Contract.Image.TABLE_NAME,[Contract.Image.COLUMN_AUTHOR_ID])
    .values(["?"]).build();
    console.log("query");
    console.log(query);
    let row = await this.connection.query(query,authorId);
    console.log(row);
    let image = new Image({
      id:row.insertId,
      location:null,
      authorId:authorId
    });
    console.log(image.toResponse());
    return image;
  }
  async updateAll(image){

    let query = new QueryBuilder().update(Contract.Image.TABLE_NAME)
      .set([Contract.Image.COLUMN_LOCATION,Contract.Image.COLUMN_AUTHOR_ID])
      .where(Contract.Image.COLUMN_ID+"=?").build();
    console.log(query);
    let rows = await this.connection.query(query,[image.location,image.authorId,image.id]);
  }
  async deleteIt(image){

    let query = new QueryBuilder().Delete()
      .from(Contract.Image.TABLE_NAME)
      .where(Contract.Image.COLUMN_ID+"=?");
    let row = await this.connection.query(query,image.id);
  }
  async attachToResource(imageId,resourceId,resourceTypeId,relationTypeId){
    let query = new QueryBuilder().insertInto(Contract.ImageToResource.TABLE_NAME,[Contract.ImageToResource.COLUMN_IMAGE_ID,
      Contract.ImageToResource.COLUMN_RESOURCE_ID,Contract.ImageToResource.COLUMN_RESOURCE_TYPE_ID,Contract.ImageToResource.COLUMN_RELATION_TYPE_ID])
      .valuesQueL(4).build();
    await this.connection.query(query,[imageId,resourceId,resourceTypeId,relationTypeId]);

  }

  async detachFromResource(imageId,resourceId,resourceTypeId,relationTypeId)
    let query = new QueryBuilder().Delete()
      .from(Contract.ImageToResource.TABLE_NAME)
      .where(Contract.ImageToResource.COLUMN_IMAGE_ID+"=?"+" AND "+ Contract.ImageToResource.COLUMN_RESOURCE_ID+"=?"+" AND "+ Contract.ImageToResource.COLUMN_RESOURCE_TYPE_ID+"=?"+" AND "+ Contract.ImageToResource.COLUMN_RELATION_TYPE_ID+"=?")
      .build();
    await this.connection.query(query,[imageId,resourceId,resourceTypeId,relationTypeId]);
  }
  async selectByResource(resourceId,resourceTypeId,relationTypeId){
    let query = new QueryBuilder().selectB(Contract.Image.TABLE_NAME,[Contract.IMage.COLUMN_ID,Contract.Image.COLUMN_LOCATION,Contract.Image.COLUMN_AUTHOR_ID])
      .from(Contract.ImageToResource.TABLE_NAME)
      .innerJoin(Contract.Image.TABLE_NAME,Contract.ImageToResource.COLUMN_IMAGE_ID)
      .where(Contract.ImageToResource.COLUMN_RESOURCE_ID+"=?"+" AND "+ Contract.ImageToResource.COLUMN_RESOURCE_TYPE_ID+"=?"+" AND "+ Contract.ImageToResource.COLUMN_RELATION_TYPE_ID+"=?")
      .build();

    let rows = await this.connection.query(query,[resourceId,resourceTypeId,relationTypeId]);
    return rowToImage(rows);
  }

function rowToImage(row){
  console.log("row");
  console.log(row);
  return new Image({
    id:row[Contract.Image.COLUMN_ID],
    location:row[Contract.Image.COLUMN_LOCATION],
    authorId:row[Contract.Image.COLUMN_AUTHOR_ID]
  });
}

function rowsToImages(rows){
  let images =[];
  for(let i=0;i<rows.length;i++)
    images.push(rowToImage(rows[i]));
  return images;
}


module.exports=ImageDBHandler;
