const mysql=require("mysql");
const dbConfig=require('../../../config/db');
const Error=require('../../../Utils/Error');


class Connection{
  constructor(connection){
    this.connection=connection;
  }
  async query(query,params){
    let promise = await new Promise((resolve,reject)=>{
      this.connection.query(query,params,(err,rows)=>{
        console.log(err);
        if(err)reject(Error.Errors.DB_ERROR);
        else resolve(rows);
      });
    }).catch(err=>{throw err});
    return promise;
  }
  async beginTransaction(){
    let promise = await new Promise((resolve,reject)=>{
      this.connection.beginTransaction((err)=>{
        if(err)reject(Error.Errors.DB_ERROR);
        else resolve();
      });
    }).catch(err=>{throw err});
    return promise;
  }
  async commit(){//TODO async mess
    var self=this;
    let promise = await new Promise((resolve,reject)=>{
      this.connection.commit(async (err)=>{
        if(err){ reject(Error.Errors.DB_ERROR);}
        else resolve();
      });
    }).catch(err=>{throw err});
    return promise;
  }
  async rollback(){
    let promise = await new Promise((resolve,reject)=>{
      this.connection.rollback((err)=>{
        if(err)reject(Error.Errors.DB_ERROR);
        else resolve();
      });
    }).catch(err=>{throw err});
    return promise;
  }
  release(){
    this.connection.release();
  }
}


module.exports=Connection;
