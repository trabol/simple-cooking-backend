const mysql=require("mysql");
const dbConfig=require('../../../config/db');
const Connection=require('./Connection');
const Pool=require('./Pool');


class MySQLWrapper{
  constructor(){}
  createPool(config){
    return new Pool(config);
  }
  createConnection(config){
    return new Connection(mysql.createConnection(config));
  }

}

module.exports=MySQLWrapper;
