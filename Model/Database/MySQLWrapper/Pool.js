const mysql=require("mysql");
const dbConfig=require('../../../config/db');
const Connection=require('./Connection');
const Error = require('../../../Utils/Error');

class Pool{
  constructor(config){
    this.pool=mysql.createPool(config);
  }
  async getConnection(){
    let promise = await new Promise((resolve,reject)=>{
      this.pool.getConnection((err,connection)=>{
        console.log("NEW CONNECTION");
        if(err)reject(Error.Errors.DB_CONNECTION_ERROR);
        else resolve(new Connection(connection));
      });
    }).catch(err=>{throw err});
    return promise;
  }

}


module.exports=Pool;
