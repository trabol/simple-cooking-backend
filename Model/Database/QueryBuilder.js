class QueryBuilder{
  constructor(query){
    this.query=query||"";
    return this;
  }
  select(args){
    this.query+="SELECT ";
    if(!(typeof args === typeof []))args=[args];
    let size = args.length;
    for(let i=0;i<size;i++){
      this.query+=args[i];
      if(i<size-1)this.query+=",";
    }
    return this;
  }
  selectB(base,args){
    this.query+="SELECT ";
    if(!(typeof args === typeof []))args=[args];
    let size = args.length;
    for(let i=0;i<size;i++){
      this.query+=base+"."+args[i];
      if(i<size-1)this.query+=",";
    }
    return this;
  }
  insertInto(tablename,args){
    this.query+="INSERT INTO ";
    this.query+=tablename;
    this.query+="(";
    if(!(typeof args === typeof []))args=[args];
    let size=args.length;
    for(let i=0;i<args.length;i++){
      this.query+=args[i];
      if(i<size-1)this.query+=",";
    }
    this.query+=")";
    return this;
  }
  insertIntoSec(table,args){
    this.query+="INSERT INTO ";
    this.query+=table;
    this.query+="(";
    let size=args.length;
    if(!(typeof args === typeof []))args=[args];
    for(let i=0;i<args.length;i++){
      this.query+=args[i];
      if(i<size-1)this.query+=",";
    }
    this.query+=")";
    return this;
  }
  Delete(){
    this.query+="DELETE ";
    return this;
  }
  update(tableName){
    this.query+="UPDATE "+tableName;
    return this;
  }
  set(args){
    this.query+=" SET ";
    if(!(typeof args === typeof []))args=[args];
    for(let i=0;i<args.length;i++){
      this.query+=args[i]+"=?";
      if(i<args.length-1)this.query+=",";
    }
    return this;
  }
  values(args){
    this.query+=" VALUES";
    this.query+="(";
    if(!(typeof args === typeof []))args=[args];
    for(let i=0;i<args.length;i++){
      this.query+=args[i];
      if(i<args.length-1)this.query+=",";
    }
    this.query+=")";
    return this;
  }
  valuesQue(args){
    this.query+=" VALUES";
    this.query+="(";
    if(!(typeof args === typeof []))args=[args];
    for(let i=0;i<args.length;i++){
      this.query+="?";
      if(i<size-1)this.query+=",";
    }
    this.query+=")";
    return this;
  }
  valuesQueL(length){
    this.query+=" VALUES";
    this.query+="(";
    for(let i=0;i<length;i++){
      this.query+="?";
      if(i<size-1)this.query+=",";
    }
    this.query+=")";
    return this;
  }
  manyValuesQue(times,numOffields){
    this.query+=" VALUES";
    for(let j=0;j<times;j++){
      this.query+="(";
      for(let i=0;i<numOffields;i++){
        this.query+="?";
        if(i<numOffields-1)this.query+=",";
      }
      this.query+=")";
      if(j<times-1)this.query+=",";
    }
    return this;
  }
  from(tableName){
    this.query+=" FROM ";
    this.query+=tableName;
    return this;
  }
  where(clause){
    this.query+=" WHERE ";
    this.query+=clause;
    return this;
  }
  having(clause){
    this.query+=" HAVING "+(clause||"");
    return this;
  }
  innerJoin(tableName){
    this.query+=" INNER JOIN ";
    this.query+=tableName;
    return this;
  }
  isNull(table,column){
    this.query+=" "+table+"."+column+" IS NULL";
    return this;
  }
  leftOuterJoin(tableName){
    this.query+=" LEFT OUTER JOIN "+tableName;
    return this;
  }
  in(args){
    if(!(typeof args === typeof []))args=[args];
    this.query+=" IN ("
    for(let i=0;i<args.length;i++){
      this.query+=args[i];
      if(i<args.length-1){
        this.query+=",";
      }
    }
    this.query+=")"
    return this;
  }
  and(query){
    this.query+=" AND "+query;
    return this;
  }
  inQue(args){
    if(!(typeof args === typeof []))args=[args];
    this.query+=" IN ("
    for(let i=0;i<args.length;i++){
      this.query+="?";
      if(i<args.length-1){
        this.query+=",";
      }
    }
    this.query+=")";
    return this;
  }
  notInQueT(table,column,lengthOfArgs){// not in filled with question marks and with given tableName and column
    this.query+=" "+table+"."+column;
    this.query+=" NOT IN ("
    for(let i=0;i<lengthOfArgs;i++){
      this.query+="?";
      if(i<lengthOfArgs-1){
        this.query+=",";
      }
    }
    this.query+=")";
    return this;
  }
  on(base1,name1,base2,name2){
    this.query+=" ON "+base1+"."+name1+"="+base2+"."+name2;
    return this;
  }
  unionAll(query){
    this.query+="\nUNION ALL\n "+(query||"");
    return this;
  }
  union(query){
    this.query+=" UNION "+(query||"");
    return this;
  }
  append(query){
    this.query+=query;
  }
  buildUpon(){
    return new QueryBuilder(this.query);
  }
  build(){
    return this.query;
  }
}

module.exports=QueryBuilder;
