let DBHandler =require( "./DBHandler");
let Contract =require( "./DBContract");
let async = require('async');
let googleStuff = require('../../services/googleVB');
let crypto = require('crypto');
let jwt=require("../../Utils/JWTPromise");
let authConfig=require("../../config/auth");
let Pair = require("../../Utils/Pair");
let Error= require('../../Utils/Error');
let User = require("../Helpers/UserModel");

class UserDBHandler extends DBHandler {
  constructor(connection,parent){
    super(connection,parent);
  }
  static get dbPairs(){
    return {
      id:new Pair("user_id","id"),
      name:new Pair("name","name"),
      email:new Pair("email","email"),
      picture:new Pair("picture","picture"),
      refreshToken:new Pair("refresh_token","refreshToken"),
      googleId:new Pair("google_id","googleId"),
      googleRefreshToken:new Pair("google_refresh_token","googleRefreshToken")
    }
  }
  async getById(id){
    let rows=await this.connection.query("SELECT "+Contract.User.COLUMN_ID+","+Contract.User.COLUMN_NAME+","+Contract.User.COLUMN_EMAIL+","+Contract.User.COLUMN_PICURE+","+Contract.User.COLUMN_REFRESH_TOKEN+","+Contract.User.COLUMN_GOOGLE_ID+","+Contract.User.COLUMN_GOOGLE_REFRESH_TOKEN+" FROM "+Contract.User.TABLE_NAME+" where "+Contract.User.COLUMN_ID+"=?",id);
    if(!rows.length)
      throw Error.Errors.USER_DOESNT_EXIST;
    else
      return new User( this.convertDataFromDatabase(rows[0]));

  }
  async checkIfExist(email) {
    let rows=await this.connection.query("SELECT 	"+Contract.User.COLUMN_ID+" FROM "+Contract.User.TABLE_NAME+" WHERE "+Contract.User.COLUMN_EMAIL+"=?",email);
    return !rows.length;
  }
  async save(user){
    let result=await this.connection.query("INSERT INTO "+Contract.User.TABLE_NAME+"("+Contract.User.COLUMN_NAME+","+Contract.User.COLUMN_EMAIL+","+Contract.User.COLUMN_GOOGLE_ID+","+Contract.User.COLUMN_PICURE+","+Contract.User.COLUMN_GOOGLE_REFRESH_TOKEN+")VALUES(?,?,?,?,?)",[user.data.name,user.data.email,user.data.googleId,user.data.picture,user.data.googleRefreshToken]);
    user.data.id=result.insertId;
  }
  async updateValue(user,DBvalueName,userValue){
    let rows=await this.connection.query("UPDATE "+Contract.User.TABLE_NAME+" SET "+DBvalueName+"=? WHERE "+Contract.User.COLUMN_ID+" = ?",[userValue,user.data.id]);
    return rows;
  }
  //TODO
  async updateValues(user,pairs){
    var query="UPDATE "+Contract.User.TABLE_NAME+" SET ";
    var values=[];
    for(let i=0;i<pairs.length;i++){
      query+=pairs[i].key+"=?";
      values.push(user.data[pairs[i].value]);
      if(i+1!=pairs.length)query+=",";
    }
    query+=" WHERE "+Contract.User.COLUMN_ID+" = ?";
    values.push(user.data.id);
    this.connection.query(query,values,function(err,result){
      callback(err,result);
    });
  }
  async updateBasic(user){
    let rows= await this.connection.query("UPDATE "+Contract.User.TABLE_NAME+" SET "+Contract.User.COLUMN_NAME+"=?,"+Contract.User.COLUMN_EMAIL+"=?,"+Contract.User.COLUMN_PICURE+"=? WHERE "+Contract.User.COLUMN_ID+" = ?",[user.data.name,user.data.email,user.data.picture,user.data.id]);
    return rows;
  }
  async updateAll(user){
    let rows = await this.connection.query("UPDATE "+Contract.User.TABLE_NAME+" SET "+Contract.User.COLUMN_NAME+"=?,"+Contract.User.COLUMN_EMAIL+"=?,"+Contract.User.COLUMN_PICURE+"=?,"+Contract.User.COLUMN_REFRESH_TOKEN+"=?,"+Contract.User.COLUMN_GOOGLE_ID+"=?,"+Contract.User.COLUMN_GOOGLE_REFRESH_TOKEN+"=? WHERE "+Contract.User.COLUMN_ID+" = ?",[user.data.name,user.data.email,user.data.picture,user.data.refreshToken,user.data.googleId,user.data.googleRefreshToken]);
    return rows;

  }
  async delete(user){
    let result = await this.connection.query("SELECT "+Contract.Dish.COLUMN_ID+" FROM "+Contract.Dish.TABLE_NAME+" WHERE "+Contract.Dish.COLUMN_AUTHOR_ID+"=?",user.data.id);
    if(result.length){
      var query1="DELETE FROM "+Contract.IngredientToDish.TABLE_NAME+" WHERE ";
      var params1 =[];
      for(var i=0;i<result.length;i++){
        query1+=Contract.IngredientToDish.COLUMN_DISH_ID+"= ?"
        params1.push(result[i][Contract.IngredientToDish.COLUMN_DISH_ID]);
        if(i!=result.length-1)query1+=" OR ";
      }

      var query2="DELETE FROM "+Contract.Dish.TABLE_NAME+" WHERE ";
      var params2 =[];
      for(var i=0;i<result.length;i++){
        query2+=Contract.Dish.COLUMN_ID+"= ?"
        params2.push(result[i][Contract.Dish.COLUMN_ID]);
        if(i!=result.length-1)query2+=" OR ";
      }
      let calls=[];
      if(params1.length)cals.push(this.connection.query(query1,params1));
      if(params2.length)cals.push(this.connection.query(query2,params2));
      let results = await Promise.all(calls);
    }
    let final = await this.connection.query("DELETE FROM "+Contract.User.TABLE_NAME+" WHERE "+Contract.User.COLUMN_ID+"= ?",user.data.id);
    return final;
    // connection.query("DELETE FROM users WHERE user_id=?",user.data.id,function(err,result){
    //   callback(err,result);
    // });
  }
  async getByGoogleId(gId){
    let rows = await this.connection.query("SELECT 	"+Contract.User.COLUMN_ID+","+Contract.User.COLUMN_NAME+","+Contract.User.COLUMN_EMAIL+","+Contract.User.COLUMN_PICURE+","+Contract.User.COLUMN_REFRESH_TOKEN+" from "+Contract.User.TABLE_NAME+" WHERE "+Contract.User.COLUMN_GOOGLE_ID+"=?",gId);
    if(!rows.length){throw Error.Errors.USER_DOESNT_EXIST}//TODO ERROR
    var user=new User( this.convertDataFromDatabase(rows[0]));
    return user;
  }
  async checkIfExistByGoogleId (gId) {
    let rows = await this.connection.query("SELECT 	"+Contract.User.COLUMN_ID+" from "+Contract.User.TABLE_NAME+" WHERE "+Contract.User.COLUMN_GOOGLE_ID+"=?",gId);
    return rows.length;
  }
  generateRefreshToken(user){
    return user.data.id+'.'+crypto.randomBytes(40).toString('hex');
  }
  async generateAccessToken(user){
    return await jwt.sign({
        exp: Math.floor(Date.now() / 1000) + (3600),
        id:user.data.id
      },authConfig.jwtSecret);
  }
  async getByAccessToken(token){
    let data = await jwt.verify(token);
    let user = await this.getById(data.id);
    return user;
  }
  async getByRefreshToken(token){
    let rows = await this.connection.query("SELECT "+Contract.User.COLUMN_ID+","+Contract.User.COLUMN_NAME+","+Contract.User.COLUMN_EMAIL+","+Contract.User.COLUMN_PICURE+","+Contract.User.COLUMN_REFRESH_TOKEN+" from "+Contract.User.TABLE_NAME+" where "+Contract.User.COLUMN_REFRESH_TOKEN+"=?",token);
    if(!rows.length){
      throw (Error.Errors.WRONG_REFRESH_TOKEN);
    }else{
      return new User( this.convertDataFromDatabase(rows[0]));
    }
  }
  convertDataFromDatabase(row){
    return{
      id:row[Contract.User.COLUMN_ID],
      name:row[Contract.User.COLUMN_NAME],
      email:row[Contract.User.COLUMN_EMAIL],
      picture:row[Contract.User.COLUMN_PICURE],
      refreshToken:row[Contract.User.COLUMN_REFRESH_TOKEN],
      googleId:row[Contract.User.COLUMN_GOOGLE_ID],
      googleRefreshToken:row[Contract.User.COLUMN_GOOGLE_REFRESH_TOKEN]
    }
  }
}

module.exports=UserDBHandler;
