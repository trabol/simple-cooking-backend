const async = require('async');
const Error= require('../../Utils/Error');
const connectionManager=require("./ConnectionManager");
const dbContract= require("./DBContract.js");
const UserDBHandler = require("./UserDBHandler");
const IngredientDBHandler = require("./IngredientDBHandler");
const DishDBHandler = require("./DishDBHandler");
const ImageDBHandler = require("./ImageDBHandler");


class DatabaseClient{
  constructor(){

  }
  async connect(){
    this.connection = await connectionManager.getConnection();
    this.User = new UserDBHandler(connection,this);
    this.Ingredient=new IngredientDBHandler(connection,this);
    this.Dish=new DishDBHandler(connection,this);
    this.Image = new ImageDBHandler(connection,this);
  }
  async beginTransaction(){
    await this.connection.get(this).beginTransaction();
  }
  async commit(){
    await this.connection.get(this).commit();
  }
  release(){
    this.connection.get(this).release();
  }
  async rollback(){
    await this.connection.get(this).rollback();
  }

}

module.exports=DatabaseClient;
