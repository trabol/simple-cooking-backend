
let Contract ={
  User:{
    TABLE_NAME:"users",
    COLUMN_ID:"user_id",
    COLUMN_NAME:"name",
    COLUMN_EMAIL:"email",
    COLUMN_PICURE:"picture",
    COLUMN_REFRESH_TOKEN:"refresh_token",
    COLUMN_GOOGLE_ID:"google_id",
    COLUMN_GOOGLE_REFRESH_TOKEN:"google_refresh_token"
  },
  Ingredient:{
    TABLE_NAME:"ingredients",
    COLUMN_ID:"ingredient_id",
    COLUMN_NAME:"ingredient_name"
  },
  Dish:{
    TABLE_NAME:"dishes",
    COLUMN_ID:"dish_id",
    COLUMN_NAME:"name",
    COLUMN_INGREDIENTS:"ingredients",
    COLUMN_RECIPE:"recipe",
    COLUMN_AUTHOR_ID:"author_id",
    COLUMN_IMAGE_ID:"image_id"
  },
  IngredientToDish:{
    TABLE_NAME:"ingredient_to_dish",
    COLUMN_DISH_ID:"dish_id",
    COLUMN_INGREDIENT_ID:"ingredient_id"
  },
  Image:{
    TABLE_NAME:"images",
    COLUMN_ID:"id",
    COLUMN_LOCATION:"location",
    COLUMN_AUTHOR_ID:"author_id"
  },
  ResourceType:{
    TABLE_NAME:"resource_types",
    COLUMN_ID:"id",
    COLUMN_NAME:"name"
  },
  ImageRelationType:{
    TABLE_NAME:"image_relation_types",
    COLUMN_ID:"id",
    COLUMN_NAME:"name"
  },
  ImageToResource:{
    TABLE_NAME:"image_to_resource",
    COLUMN_IMAGE_ID:"image_id",
    COLUMN_RESOURCE_ID:"resource_id",
    COLUMN_RELATION_TYPE_ID:"relation_type_id",
    COLUMN_RESOURCE_TYPE_ID:"resource_type_id"
  }
}

module.exports=Contract;
