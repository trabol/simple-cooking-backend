var User= require("../Model/Helpers/UserModel");
var async=require("async");
var googleStuff=require("../services/googleVB");

var Controller=require("../Utils/Controller");
var Activity = require("../Utils/Activity");
var Error= require('../Utils/Error');
var QueryParam=require("../Utils/QueryParam");

var authController = new Controller();
authController.name="auth";

var google = new Activity();//TODO google error handler
google.name="google/callback";
google.method=Activity.Methods.Get;
google.neededData=[new QueryParam("code",QueryParam.Locations.QUERY)];
google.authenticationLevel=Activity.AuthLevels.Guest;
google.needsDB=true;
google.needsErrorHandler=true;
google.task=async function(req,res,next){
  console.log("tasks");
  //let user = res.locals.user;
  let DB = res.locals.DB;
  try{
    let response;
    try{
      oauth2Client=googleStuff.getOAuthClient();
      console.log("we have client");
      const {tokens} = await oauth2Client.getToken(res.locals.data.code);
      console.log("we have tokens");
      //console.log(tokens);
      oauth2Client.setCredentials(tokens);
      console.log("oauth ");
      //console.log(oauth2Client.credentials);
      response = await googleStuff.plus.people.get({ userId: 'me', auth: oauth2Client });
    }catch(err){
      console.log("oauth error");
      console.log(err);
      if(!err.code)err = Error.Errors.GOOGLEAUTHERROR;
      throw err;
    }
    console.log("we have response");
    console.log(response.data);
    let user;
    try{
      user = await DB.User.getByGoogleId(response.data.id);
    }catch(err){
      console.log("err");
      console.log(err);
      if(err.code===Error.Code.USER_DOESNT_EXIST){
        console.log("doesnt exists");
        user=new User({
          googleId:response.data.id,
          googleRefreshToken:oauth2Client.credentials.refresh_token,
          name:response.data.displayName,
          email:response.data.emails[0].value,
          picture:response.data.image.url,
        });
        try{
          user.data.refreshToken=DB.User.generateRefreshToken(user);
          console.log("begin transaction");
          await DB.beginTransaction();
          await DB.User.save(user);
          await DB.User.updateValue(user,"refresh_token",user.data.refreshToken);
          await DB.commit();
          console.log("commited");
        }catch(err){
          console.log("dbErrpr");
          console.log(err);
          await DB.rollback();
          throw err;
        }
      }else{
        console.log("throwing up db");
        throw err;
      }
    }
    user.data.accessToken=await DB.User.generateAccessToken(user);
    res.json({
      user:user.toResponse()
    });

  }catch(err){
    next(err);
  }finally{
    res.locals.DB.release();
  }
};
google.localMiddlewares.push(function serialize(req,res,next){
  var data={
    code:req.query.code
  }
  res.locals.data=data;
    next();
});

var refreshAccessToken = new Activity();
refreshAccessToken.name='refreshaccesstoken';
refreshAccessToken.method=Activity.Methods.Get;
refreshAccessToken.neededData=[new QueryParam("refresh_token",QueryParam.Locations.QUERY)];
refreshAccessToken.authenticationLevel=Activity.AuthLevels.Guest;
refreshAccessToken.needsDB=true;
refreshAccessToken.needsErrorHandler=true;
refreshAccessToken.task =async function(req,res,next){
  let DB = res.locals.DB;
  try{
    let user = await DB.User.getByRefreshToken(res.locals.data.refreshToken);
    user.data.accessToken = await DB.User.generateAccessToken(user);
    res.json({
      user:user.toResponse()
    });
  }catch(err){
    next(err);
  }
};
refreshAccessToken.localMiddlewares.push( function serialize(req,res,next){
  var data={
    refreshToken:req.query.refresh_token
  }
  res.locals.data=data;
    next();
});

authController.activities.push(google);
authController.activities.push(refreshAccessToken);

module.exports= authController;
