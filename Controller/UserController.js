var User= require("../Model/Helpers/UserModel");
var Dish= require("../Model/Helpers/DishModel");
var async=require("async");
var Controller=require("../Utils/Controller");
var Activity = require("../Utils/Activity");
var Error= require('../Utils/Error');
var QueryParam=require("../Utils/QueryParam");
var userController=new Controller();
userController.name="user";

var get = new Activity();
get.name=":id";
get.method=Activity.Methods.Get;
get.authenticationLevel=Activity.AuthLevels.Guest;
get.needsDB=true;
get.needsErrorHandler=true
get.localMiddlewares.push(function dataSerializer(req,res,next){
  res.locals.data={
    user:{
      id:req.params.id
    }
  };
  next();
});
get.task=async function(req,res,next){
  try{
    let DB = res.locals.DB;
    let user = await DB.User.getById(res.locals.data.user.id);
    res.json({
      user:user.toResponse(true)
    });
  }catch(err){
    next(err);
  }finally{
    res.locals.DB.release();
  }
};

var put = new Activity();
put.name=":id"
put.method=Activity.Methods.Put;
put.authenticationLevel=Activity.AuthLevels.User;
put.neededData=[new QueryParam("name",QueryParam.Locations.BODY),new QueryParam("email",QueryParam.Locations.BODY),new QueryParam("picture",QueryParam.Locations.BODY)];
put.localMiddlewares.push(function dataSerializer(req,res,next){
  res.locals.data={
    user:{
      id:res.params.id,
      name:req.body.name,
      email:req.body.email,
      picture:req.body.picture
    }
  };
  next();
});
put.task=async function(req,res,next){
  try{
    let DB = res.locals.DB;
    let user=new User(res.locals.data.user);
    await DB.User.getById(user.data.id);
    await DB.User.updateBasic(user);
    res.json({
      user:user.toResponse()
    })
  }catch(err){
    next(err);
  }finally{
    res.locals.DB.release()
  }
}

var getDishes = new Activity();
getDishes.name=":id/dishes"
getDishes.method=Activity.Methods.Get;
getDishes.authenticationLevel=Activity.AuthLevels.Guest;
getDishes.needsDB=true;
getDishes.needsErrorHandler=true;
getDishes.localMiddlewares.push(function(req,res,next){
  res.locals.data={
    id:req.params.id
  };
  next();
});
getDishes.task=async function(req,res,next){
  try{
    let DB = res.locals.DB;
    let dishes = await DB.Dish.getByAuthorId(res.locals.data.id);
    let responseDishes= [];
    for(let i=0;i<dishes.length;i++){
      responseDishes.push(dishes[i].toResponse());
    }
    res.json({
      dishes:responseDishes
    });
  }catch(err){
    next(err);
  }finally{
    res.locals.DB.release()
  }
}



userController.activities.push(get);
userController.activities.push(put);
userController.activities.push(getDishes);



module.exports= userController;
