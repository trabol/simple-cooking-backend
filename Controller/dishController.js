var Ingredient= require("../Model/Helpers/IngredientModel");
var Dish= require("../Model/Helpers/DishModel");
var User= require("../Model/Helpers/UserModel");
let Image = require("../Model/Helpers/ImageModel");
var Controller=require("../Utils/Controller");
var Activity = require("../Utils/Activity");
var Error= require('../Utils/Error');
var QueryParam=require("../Utils/QueryParam");



var dishController = new Controller();
dishController.name="dish";

var post = new Activity();
post.method=Activity.Methods.Post;
post.authenticationLevel=Activity.AuthLevels.User;
post.neededData=[new QueryParam("name",QueryParam.Locations.BODY),new QueryParam("recipe",QueryParam.Locations.BODY),new QueryParam("ingredient",QueryParam.Locations.BODY)];
post.needsDB=true;
post.needsErrorHandler=true;
post.localMiddlewares.push(function dataSerializer(req,res,next){
  var data={
    dish:{
					name:req.body.name,
					ingredients:new Array(),
					recipe:req.body.recipe,
					author_id:null,
				}
  }
  if(typeof(req.body.ingredient)==typeof("s"))data.dish.ingredients.push(new Ingredient({id:null,name:req.body.ingredient}));
  else
    for(let i=0;i<req.body.ingredient.length;i++){
			var ing = new Ingredient({id:null,name:req.body.ingredient[i]});
			data.dish.ingredients.push(ing);
		}
  res.locals.data=data;
  next();
});
post.task= async function(req,res,next){
  try{
    let DB = res.locals.DB;
    let user = res.locals.user;
    let ingredients = await DB.Ingredient.getIngredientsByName(res.locals.data.dish.ingredients);
    let dish = new Dish(res.locals.data.dish);
    dish.data.ingredients=ingredients;
    dish.data.authorId=user.data.id;
    await DB.beginTransaction();
    dish = await DB.Dish.save(dish);
    await DB.commit();
    DB.release();
    res.json({
      dish:dish.toResponse()
    });
  }catch(err){
    await res.locals.DB.rollback();
    res.locals.DB.release();
    next(err);
  }
}



var get =new Activity();
get.name=":id";
get.method=Activity.Methods.Get;
get.authenticationLevel=Activity.AuthLevels.Guest;
get.needsDB=true;
get.needsErrorHandler=true;
get.localMiddlewares.push(function(req,res,next){
  var data={
    dish:{
          id:req.params.id
        }
  }
  res.locals.data=data;
  next();
});
get.task= async function(req,res,next){
  try{
    let DB=res.locals.DB;
    let dish = await DB.Dish.getById(res.locals.data.dish.id);
    res.json({
      dish:dish.toResponse()
    });
  }catch(err){
    next(err);
  }finally{
    res.locals.DB.release();
  }
}


var put = new Activity();//TODO ingredients as array of names
put.name=":id";
put.method=Activity.Methods.Put;
put.authenticationLevel=Activity.AuthLevels.User;
put.neededData=[new QueryParam("name",QueryParam.Locations.BODY),new QueryParam("recipe",QueryParam.Locations.BODY),new QueryParam("ingredient",QueryParam.Locations.BODY)];
put.needsDB=true;
put.needsErrorHandler=true;
put.localMiddlewares.push(function dataSerializer(req,res,next){
  var data={
    dish:{
          id:req.params.id,
					name:req.body.name,
					ingredients:new Array(),
					recipe:req.body.recipe,
					author_id:null,
				}
  }
  if(typeof(req.body.ingredient)==typeof("s"))data.dish.ingredients.push(new Ingredient({id:null,name:req.body.ingredient}));
  else {
    for(let i=0;i<req.body.ingredient.length;i++){
  			var ing = new Ingredient({id:null,name:req.body.ingredient[i]});
  			data.dish.ingredients.push(ing);
  		}
  }
    res.locals.data=data;
    console.log(data.dish.ingredients);
    next();
});
put.task=async function(req,res,next){
  try{
    let DB = res.locals.DB;
    await DB.beginTransaction();
    let user = res.locals.user;
    let newDish = new Dish(res.locals.data.dish);
    let oldDish ={};
    let ingredients=await DB.Ingredient.getIngredientsByName(newDish.data.ingredients);
    newDish.data.ingredients=ingredients;
    oldDish=await DB.Dish.getById(newDish.data.id);
    if(user.data.id!==oldDish.data.authorId)
      throw Error.Errors.NOT_YOUR_DISH;
    await DB.Dish.updateAll(oldDish,newDish);
    await DB.commit();
    DB.release();
    res.json({
      dish:newDish.toResponse()
    });
  }catch(err){
    await res.locals.DB.rollback();
    res.locals.DB.release();
    next(err);
  }
};

var deleteAct= new Activity();
deleteAct.name=":id"
deleteAct.method=Activity.Methods.Delete;
deleteAct.authenticationLevel=Activity.AuthLevels.User;
deleteAct.needsDB=true;
deleteAct.needsErrorHandler=true;
deleteAct.localMiddlewares.push(function(req,res,next){
  res.locals.data={
    dish:{
      id:req.params.id
    }
  }
  next();
});

deleteAct.task= async function(req,res,next){
  try{
    let DB=res.locals.DB;
    await DB.beginTransaction();
    let user = res.locals.user;
    let dish = await DB.Dish.getById(res.locals.data.dish.id);
    if(user.data.id!==dish.data.authorId)
      throw(Error.Errors.NOT_YOUR_DISH);
    await DB.Dish.delete(dish);
    DB.commit();
    DB.release();
    res.json({
      dish:dish.toResponse()
    });
  }catch(err){
    await res.locals.DB.rollback()
    res.locals.DB.release();
    next(err);
  }
}

dishController.activities.push(post);
dishController.activities.push(get);
dishController.activities.push(put);
dishController.activities.push(deleteAct);

module.exports= dishController;
