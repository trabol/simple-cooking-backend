var Ingredient= require("../Model/Helpers/IngredientModel");
var Dish= require("../Model/Helpers/DishModel");
var User= require("../Model/Helpers/UserModel");
var async=require("async");
var Controller=require("../Utils/Controller");
var Activity = require("../Utils/Activity");
var Error= require('../Utils/Error');
var QueryParam=require("../Utils/QueryParam");

var ingredientController=new Controller();
ingredientController.name="ingredient";

var post = new Activity();
post.method=Activity.Methods.Post;
post.authenticationLevel=Activity.AuthLevels.User;
post.needsDB=true;
post.needsErrorHandler=true;
post.neededData=[new QueryParam("name",QueryParam.Locations.BODY)];
post.localMiddlewares.push(function(req,res,next){
  var data={
    ingredient:{
					id:null,
					name:req.body.name
				}
  }
  res.locals.data=data;
    next();
});
post.task=async function(req,res,next){
  try{
    let DB=res.locals.DB;
    let user = res.locals.user;
    let ingredient = new Ingredient(res.locals.data.ingredient)
    let exists;
    try{
      await DB.Ingredient.getByName(ingredient.data.name);
      exists=true;
    }catch(err){
      if(err.code===Error.Errors.INGREDIENT_DOESNT_EXIST.code)
        exists=false;
      else
        throw err;
    }
    if(exists)
      throw Error.Errors.INGREDIENT_ALLREADY_EXISTS;
    ingredient=await DB.Ingredient.save(ingredient);
    res.json({
      ingredient:ingredient.toResponse()
    });
  }catch(err){
    next(err)
  }finally{
    res.locals.DB.release();
  }
};

var get = new Activity();
get.name=":id";
get.method=Activity.Methods.Get;
get.authenticationLevel=Activity.AuthLevels.Guest;
get.needsDB=true;
get.needsErrorHandler=true;
get.localMiddlewares.push(function dataSerializer(req,res,next){
  res.locals.data={id:req.params.id};
  next();
});
get.task= async function(req,res,next){
  try{
    let DB=res.locals.DB;
    let ingredient = await DB.Ingredient.getById(res.locals.data.id);
    res.json({
      ingredient:ingredient
    });
  }catch(err){
    next(err);
  }finally{
    res.locals.DB.release();
  }
};

var put = new Activity();//TODO Move to CMS
put.name=":id"
put.method=Activity.Methods.Put;
put.authenticationLevel=Activity.AuthLevels.IngredientModerator;
put.neededData=[new QueryParam("name",QueryParam.Locations.BODY)];
put.needsDB=true;
put.needsErrorHandler=true;
put.localMiddlewares.push(function dataSerializer(req,res,next){
  res.locals.data={
    ingredient:{
      id:req.params.id,
      name:req.body.name
    }
  };

  next();
});
put.task=async function(req,res,next){
  try{
    let DB=res.locals.DB;
    let ingredient=new Ingredient(res.locals.data.ingredient);
    await DB.Ingredient.getById(ingredient.data.id);
    await DB.Ingredient.update(ingredient);
    res.json({
      ingredient:ingredient.toResponse()
    });
  }catch(err){
    next(err);
  }finally{
    res.locals.DB.release();
  }
}

var getByName=new Activity();
getByName.name="name/:name";
getByName.method=Activity.Methods.Get;
getByName.authenticationLevel=Activity.AuthLevels.Guest;
getByName.needsDB=true;
getByName.needsErrorHandler=true;
getByName.localMiddlewares.push(function dataSerializer(req,res,next){
  res.locals.data={
    ingredient:{
      id:null,
      name:req.params.name
    }
  };
  next();
});
getByName.task=async function(req,res,next){
  try{
    let DB=res.locals.DB;
    let ingredient = await DB.Ingredient.getByName(res.locals.data.ingredient.name);
    res.json({
      ingredient:ingredient
    });
  }catch(err){
    next(err);
  }finally{
    res.locals.DB.release();
  }
}

ingredientController.activities.push(post);
ingredientController.activities.push(get);
ingredientController.activities.push(put);
ingredientController.activities.push(getByName);



module.exports= ingredientController;
