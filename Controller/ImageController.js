var Ingredient= require("../Model/Helpers/IngredientModel");
var Dish= require("../Model/Helpers/DishModel");
var User= require("../Model/Helpers/UserModel");
var async=require("async");
var Controller=require("../Utils/Controller");
var Activity = require("../Utils/Activity");
var Error= require('../Utils/Error');
var QueryParam=require("../Utils/QueryParam");
const ImageUploader = require("../Model/ImageUploader/ImageUploader.js");
var imageController=new Controller();
imageController.name="image";

var upload = new Activity();
upload.name="upload";
upload.method=Activity.Methods.Post;
upload.authenticationLevel=Activity.AuthLevels.Guest;
upload.needsDB=true;
upload.needsErrorHandler=true;
upload.localMiddlewares.push(function(req,res,next){
  next()
});

upload.task=async function(req,res,next){
  try{
    let DB=res.locals.DB;
    await DB.beginTransaction();
    let image = await DB.Image.create(/*res.locals.user.data.id*/15);
    console.log("IMAGE");
    console.log(image);
    image.location =await ImageUploader.upload(req,res,"image",image.id+".png");
    console.log("UPLOADING FINISHED");
    await DB.Image.updateAll(image);
    await DB.commit();
    res.json({
      image:image
    });
  }catch(err){
    await res.locals.DB.rollback();
    next(err)
  }finally{
    res.locals.DB.release();
  }
};

var get = new Activity();
get.name=":id";
get.method=Activity.Methods.Get;
get.authenticationLevel=Activity.AuthLevels.Guest;
get.needsDB=true;
get.needsErrorHandler=true;
get.localMiddlewares.push(function dataSerializer(req,res,next){
  res.locals.data={id:req.params.id};
  next();
});
get.task= async function(req,res,next){
  try{
    let DB=res.locals.DB;
    let image = await DB.Image.getById(res.locals.data.id);
    res.json({
      image:image
    });
  }catch(err){
    next(err);
  }finally{
    res.locals.DB.release();
  }
};

var put = new Activity();//TODO Move to CMS
put.name=":id"
put.method=Activity.Methods.Put;
put.authenticationLevel=Activity.AuthLevels.IngredientModerator;
put.neededData=[new QueryParam("name",QueryParam.Locations.BODY)];
put.needsDB=true;
put.needsErrorHandler=true;
put.localMiddlewares.push(function dataSerializer(req,res,next){
  res.locals.data={
    ingredient:{
      id:req.params.id,
      name:req.body.name
    }
  };

  next();
});
put.task=async function(req,res,next){
  try{
    let DB=res.locals.DB;
    let ingredient=new Ingredient(res.locals.data.ingredient);
    await DB.Ingredient.getById(ingredient.data.id);
    await DB.Ingredient.update(ingredient);
    res.json({
      ingredient:ingredient.toResponse()
    });
  }catch(err){
    next(err);
  }finally{
    res.locals.DB.release();
  }
}

imageController.activities.push(upload);
imageController.activities.push(get);
imageController.activities.push(put);



module.exports= imageController;
