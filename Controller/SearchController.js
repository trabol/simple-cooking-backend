var Ingredient= require("../Model/Helpers/IngredientModel");
var Dish= require("../Model/Helpers/DishModel");
var User= require("../Model/Helpers/UserModel");

var Controller=require("../Utils/Controller");
var Activity = require("../Utils/Activity");
var Error= require('../Utils/Error');
var QueryParam=require("../Utils/QueryParam");

var searchController = new Controller();
searchController.name="search";

var simple = new Activity();
simple.name="simple";
simple.method=Activity.Methods.Get;
simple.authenticationLevel=Activity.AuthLevels.Guest;
simple.neededData=[new QueryParam("ingredient",QueryParam.Locations.QUERY)];
simple.needsDB=true;
simple.needsErrorHandler=true;
simple.localMiddlewares.push(function serializer(req,res,next){
  var data={
      ingredients:new Array()
  }
  if(typeof(req.query.ingredient)===typeof("s"))data.ingredients.push(new Ingredient({id:null,name:req.query.ingredient}));
  else
    for(var i=0;i<req.query.ingredient.length;i++){
      var ing = new Ingredient({id:null,name:req.query.ingredient[i]});
      data.ingredients.push(ing);
    }
  res.locals.data=data;
    next();
});

simple.task=async function(req,res,next){
  try{
    let DB=res.locals.DB;
    let user = res.locals.user;
    let ingredients = await DB.Ingredient.getIngredientsByName(res.locals.data.ingredients);
    let dishes = await DB.Dish.getByIngredients(ingredients);
    let ingredientsWithKey = await DB.Dish.getIngredientsForManyDishes(dishes);
    let dishesMap=new Map();
    for(let i=0;i<dishes.length;i++){
      dishesMap.set(dishes[i].data.id,dishes[i]);
    }
    for(let i=0;i<ingredientsWithKey.length;i++){
      dishesMap.get(ingredientsWithKey[i][0]).data.ingredients.push(ingredientsWithKey[i][1]);
    }
    let responseDishes=[];
    for(let i=0;i<dishes.length;i++)
      responseDishes.push(dishesMap.get(dishes[i].data.id).toResponse());
    res.json({
      dishes:responseDishes
    });
  }catch(err){
    next(err);
  }finally{
    res.locals.DB.release();
  }
};

searchController.activities.push(simple);

module.exports= searchController;
