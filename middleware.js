var googleStuff=require('./config/google');
var async= require('async');
var connection=require('./Model/Helpers/database');
var User = require('./Model/Helpers/UserModel');
var Error= require('./Utils/Error');
const DatabaseClient=require('./Model/Database/DatabaseClient');

var getDB =function(){
  return async function(req,res,next){
    res.locals.DB=new DatabaseClient();
    await res.locals.DB.connect();
    next();
  }
}

//rewrite for my own accessTokens
var authenticate = function(role){

  return async function(req,res,next){
    try{
      var access_token = req.body.access_token || req.body.access_token || req.headers['access_token'];
      if(access_token){
        let user = await res.locals.DB.User.getByAccessToken(access_token);
        res.locals.user=user;
        next();
      }else{
        if(role>0){
          throw Error.Errors.NO_ACCESS_TOKEN;
        }else{
          res.locals.user= new User({role:0,isAuthenticated:false});
          next();
        }
      }
    }catch(err){
      next(err);
    }
  }
}


var checkData = function(table){
  return function(req,res,next){
    var hasAll=true;
    for(let i=0;i<table.length;i++){
      if(!(req[table[i].location][table[i].name])){
        hasAll=false;
      }
    }
    if(hasAll){
      next();
    }else{
      err=Error.Errors.NOT_ENOUGH_DATA;
      next(err);
      // res.status(error.status).json({
      //   error:error
      // });
    }
  }

}

var errorHandler=function(err,req,res,next){
  console.log("handler");
  console.log(err);
  if(err){
    if(err.status)
      res.status(err.status).json({error:err});
    else{
      //TODO store errors somewhere!!!
      res.status(500).json({error:Error.Errors.INTERNAL});
    }
  }
  // }else{
  //   err=Error.Errors.NOT_FOUND;
  // }
  // res.status(404).json({
  //   error:err
  // });
};
module.exports={
  getDB:getDB,
  authenticate:authenticate,
  checkData:checkData,
  errorHandler:errorHandler
}
