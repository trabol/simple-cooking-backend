var credentials={
	acquireTimeout:100*60*5*10,
	connectionLimit : 100,
	host     : 'db4free.net',
  user     : 'scookingdbadmin',
  password : 'WIhbydGEN#T7138M*T3e6n3tm&#n^Y28em',
  database : 'simplecooking',
	port:3306

};

var openShift={
	acquireTimeout:100*60*5*10,
	connectionLimit : 100,
	host     : 'mysql://mysql:3306/',
  user     : 'user068',
  password : 'AfLoUxNURqjShEAa',
  database : 'simple-cooking',
	port:3306

};

var Localcredentials={
	acquireTimeout:100*60*5*10,
	connectionLimit : 10,
	host     : 'localhost',
  user     : 'localdbadmin',
  password : 'localdbadmin626',
  database : 'simple-cookingdb',
	//port:3306

};

var LocalcredentialsPC={
	acquireTimeout:100*60*5*10,
	connectionLimit : 10,
	host     : 'localhost',
  user     : 'simple-cookingdbadmin',
  password : 'simple-cookingdbadmin626',
  database : 'simple-cookingdb',
	//port:3306

};

var startupQueries=[];
//startupQueries.push("DROP TABLE IF EXISTS users");

startupQueries.push("CREATE TABLE IF NOT EXISTS `image_to_resource` (  `image_id` int(10) UNSIGNED NOT NULL , `resource_id` int(10) UNSIGNED NOT NULL,  `relation_type_id` int(10) UNSIGNED NOT NULL,  `resource_type_id` int(10) UNSIGNED NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=latin1;");

startupQueries.push("CREATE TABLE IF NOT EXISTS `image_relation_types` (  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, `name` varchar(20) NOT NULL,   PRIMARY KEY (`id`)) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;");

startupQueries.push("CREATE TABLE IF NOT EXISTS `resource_types` (  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, `name` varchar(10) NOT NULL,   PRIMARY KEY (`id`)) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;");


startupQueries.push("CREATE TABLE IF NOT EXISTS `dishes` (  `dish_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, `name` varchar(50) NOT NULL,  `recipe` text NOT NULL,  `image_id` varchar(250) DEFAULT NULL,  `author_id` int(11) NOT NULL,  PRIMARY KEY (`dish_id`)) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;");

startupQueries.push("CREATE TABLE IF NOT EXISTS `fridges` (  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,  `name` varchar(50) NOT NULL,  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,  PRIMARY KEY (`id`)) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;");

startupQueries.push("CREATE TABLE IF NOT EXISTS `fridge_to_ingredient` (  `fridge_id` int(10) UNSIGNED NOT NULL,  `ingredient_id` int(10) UNSIGNED NOT NULL,  UNIQUE KEY `fridge_id` (`fridge_id`,`ingredient_id`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;");

startupQueries.push("CREATE TABLE IF NOT EXISTS `fridge_to_user` (  `fridge_id` int(10) UNSIGNED NOT NULL,  `user_id` int(10) UNSIGNED NOT NULL,  UNIQUE KEY `fridge_id` (`fridge_id`,`user_id`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;");

startupQueries.push("CREATE TABLE IF NOT EXISTS `ingredients` (  `ingredient_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,  `ingredient_name` varchar(50) NOT NULL,  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,  PRIMARY KEY (`ingredient_id`)) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;");

startupQueries.push("CREATE TABLE IF NOT EXISTS `ingredient_to_dish` (  `ingredient_id` int(11) NOT NULL,  `dish_id` int(11) NOT NULL,  PRIMARY KEY (`ingredient_id`,`dish_id`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;");

startupQueries.push("CREATE TABLE IF NOT EXISTS `users` (  `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,  `name` varchar(50) NOT NULL,  `email` varchar(60) NOT NULL,  `google_id` varchar(100) NOT NULL,  `picture` varchar(250) NOT NULL,  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,  `google_refresh_token` varchar(255) NOT NULL,  `refresh_token` varchar(100) ,  PRIMARY KEY (`user_id`),  UNIQUE KEY `google_email` (`email`)) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;");

startupQueries.push("CREATE TABLE IF NOT EXISTS `images` (  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, `location` varchar(250) DEFAULT NULL,  `author_id` int(11) NOT NULL,  PRIMARY KEY (`id`)) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;");


//basic Data

startupQueries.push("INSERT INTO `resource_types` (`id`, `name`) VALUES (NULL, 'user'), (NULL, 'dish'), (NULL, 'ingredient')");

startupQueries.push("INSERT INTO `image_relation_types` (`id`, `name`) VALUES (NULL, 'profile_picture'), (NULL, 'main_picture');");

//startupQueries.push("INSERT INTO `users` (`user_id`, `name`, `email`, `google_id`, `picture`, `created`, `google_refresh_token`, `refresh_token`) VALUES (NULL, 'Kacper Tr?bacz', 'trebacz.kacper@gmail.com', '117866508282124806716', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', '2017-03-09 15:11:51', '1/n7k452aON9pOMGCwf9xpW5VOsl7UxhpNz6etIL3uH1U', '15.93beb1c6164451269053265aa27e8d38f11fc3e7394f36493e294bc921c74f2a3fa38edffb7f31b6')");
module.exports = {
  credentials:Localcredentials,
  startupQueries:startupQueries
};
