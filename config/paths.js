var PATHS={
   addRootDirectory(root,httpRoot){
    this.local={
      root:root,
      images:root+"public\\images\\"
    }
    this.http={
      root:httpRoot,
      images:httpRoot+"images/"
    }
  }
}

module.exports=PATHS;
