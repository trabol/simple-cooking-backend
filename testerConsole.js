require("./config/paths").addRootDirectory(__dirname,"http://localhost:8080/");
console.log(require("./config/paths"));
var ImageService = require("./services/ImageService");

var  User = require("./Model/Helpers/UserModel");
 var  Ingredient = require("./Model/Helpers/IngredientModel");
 var Dish=require("./Model/Helpers/DishModel");
//
const dbConfig = require('./config/db');
const MySQLWrapper = require('./Model/Database/MySQLWrapper/MySQl');
let mysql=new MySQLWrapper();
const DatabaseClient=require('./Model/Database/DatabaseClient');
const ConnectionManager=require('./Model/Database/ConnectionManager') ;
let QueryBuilder = require("./Model/Database/QueryBuilder");
let Contract = require("./Model/Database/DBContract");

// async function test(){
//   try{
//     let inputIds=[1,2,32,23,5435,32];
//     let query= new QueryBuilder().select([Contract.Dish.TABLE_NAME+"."+Contract.Dish.COLUMN_ID,Contract.Dish.TABLE_NAME+"."+Contract.Dish.COLUMN_NAME,Contract.Dish.TABLE_NAME+"."+Contract.Dish.COLUMN_RECIPE,Contract.Dish.TABLE_NAME+"."+Contract.Dish.COLUMN_AUTHOR_ID])
//       .from(Contract.Dish.TABLE_NAME)
//       .leftOuterJoin(Contract.IngredientToDish.TABLE_NAME)
//         .on(Contract.Dish.TABLE_NAME,Contract.Dish.COLUMN_ID,Contract.IngredientToDish.TABLE_NAME,Contract.IngredientToDish.COLUMN_DISH_ID)
//         .and("").notInQueT(Contract.IngredientToDish.TABLE_NAME,Contract.IngredientToDish.COLUMN_INGREDIENT_ID,inputIds.length)
//         .where("").isNull(Contract.IngredientToDish.TABLE_NAME,Contract.IngredientToDish.COLUMN_DISH_ID);
//         console.log(query);
//     }catch(err){
//       console.log(err);
//     }
// }
// test();


// async function mysqlwrappertest(){
//   console.log("start wrapper");
//   try{
//   let pool= mysql.createPool(dbConfig.credentials);
//   let connection=await pool.getConnection();
//   await connection.beginTransaction();
//   let rows=await connection.query("SELECT email FROM users WHERE user_id=?",1);
//   await connection.commit();
//   connection.release();
//   console.log("mysqlwrappertest "+Date.now());
//   console.log(rows[0]);
//   }catch(err){
//     console.log(err);
//   }
// }
// async function databaseClientTest(){
// async function connectionMannagerTest(){
//   console.log("start connectionManager");
//   try{
//   let connection= await ConnectionManager.getConnection();
//   await connection.beginTransaction();
//   let rows=await connection.query("SELECT email FROM users WHERE user_id=?",1);
//   await connection.commit();
//   connection.release();
//   console.log("connectionMannagerTest "+Date.now());
//   console.log(rows[0]);
//   }catch(err){
//   console.log(err);
//   }
// }
//   console.log("start databaseClientTest");
//   try{
//     let db = new DatabaseClient();
//     await db.create();
//     db.release();
//     console.log("db client created "+Date.now());
//   }catch(err){
//     console.log("DatabaseClientTest");
//     console.log(err);
//   }
// }
//
// async function getByUserIdTest(){
//   console.log("start getuserbyid");
//   try{
//     let db = new DatabaseClient();
//     await db.create();
//     console.log("DB created");
//     let user = await db.User.getById(1);
//     db.release();
//     console.log(Date.now());
//     console.log(user.toResponse());
//   }catch(err){
//     console.log(err);
//   }
// }
//
// async function getIngredientById(){
//   try{
//     let db = new DatabaseClient();
//     await db.create();
//     console.log("DB created");
//     let ingredient = await db.Ingredient.getById(14);
//     db.release();
//     console.log(Date.now());
//     console.log(ingredient.toResponse());
//   }catch(err){
//     console.log(err);
//   }
// }
//
//
//
// async function query(q,p){
//   console.log("start connectionManager");
//   try{
//     let connection= await ConnectionManager.getConnection();
//     await connection.beginTransaction();
//     let rows=await connection.query(q,p);
//     await connection.commit();
//     connection.release();
//     console.log("connectionMannagerTest "+Date.now());
//     return rows;
//   }catch(err){
//     console.log(err);
//   }
// }
//
// async function getIngredientById(){
//   try{
//     let db = new DatabaseClient();
//     console.log("before");
//     await db.create();
//     console.log("DB created");
//     let dishes=[];
//     dishes.push(new Dish({id:58,ingredients:[]}));
//     //dishes.push(new Dish({id:62,ingredients:[]}));
//     let ingredients = await db.Dish.getIngredientsForManyDishes(dishes);
//     let dishesMap=new Map();
//     for(let i=0;i<dishes.length;i++){
//       dishesMap.set(dishes[i].data.id,dishes[i]);
//     }
//     console.log(ingredients);
//     for(let i=0;i<ingredients.length;i++){
//       dishesMap.get(ingredients[i][0]).data.ingredients.push(ingredients[i][1]);
//     }
//     db.release();
//     console.log(Date.now());
//     console.log(dishesMap);
//     console.log(dishesMap.get(58).toResponse());
//   }catch(err){
//     console.log(err);
//   }
// }
//
// async function getByUserIdTest(){
//   console.log("start getuserbyid");
//   try{
//     let db = new DatabaseClient();
//     await db.create();
//     console.log("DB created");
//     let user = await db.User.getById(1);
//     db.release();
//     console.log(Date.now());
//     console.log(user.toResponse());
//   }catch(err){
//     console.log(err);
//   }
// }
//
// // console.log(new QueryBuilder().insertInto(Contract.IngredientToDish.TABLENAME,[Contract.IngredientToDish.COLUMN_DISH_ID,Contract.IngredientToDish.COLUMN_INGREDIENT_ID])
// //   .manyValuesQue(7,2).build());
//
async function getDishesByIngredients(){
  let ingredients=[new Ingredient({id:1})];

  try{
    let db = new DatabaseClient();
    await db.connect();
    console.log("DB created");
    let dishes = await db.Dish.getByIngredients(ingredients);
    db.release();
    console.log(dishes);
  }catch(err){
    console.log(err);
  }
}
getDishesByIngredients();
//
// async function getIngredientsByName(){
//   let ingredients=[new Ingredient({name:"egg"}),
//     new Ingredient({name:"pasta"}),
//   new Ingredient({name:"ketchup"})];
//
//   try{
//     let db = new DatabaseClient();
//     await db.create();
//     console.log("DB created");
//     let rows = await db.Ingredient.getIngredientsByName(ingredients);
//     db.release();
//     console.log(rows);
//   }catch(err){
//     console.log(err);
//   }
// }
// //getByUserIdTest();
// //selectIngredientsForMultipledishes();
//  getIngredientsByName();
// //query();
// //getIngredientById();
// /*var user = new User({
//   id:5,
//   name:"Kacper",
//   email:"email@mail.com",
//   refreshToken:"12345"
// });
// console.log("test 1");
//  Dish.getById(42,function(err,dish){
//   if(err){
//     console.log(err);
//     return;
//   }
//   //console.log(dish);
//   dish.delete(function(err){
//     if(err)console.log(err);
//     else console.log("succesfully deleted");
//   });
//
// });
//
// console.log("test 2");
// Dish.getById(57,function(err,dishNew){
//  if(err){
//    console.log(err);
//    return;
//  }
//
//  dishNew.data.id=47;
//  Dish.getById(dishNew.data.id,function(err,dishOld){
//    if(err){console.log(errr);return;}
//    console.log(dishOld.toResponse());
//    dishNew.updateAll(dishOld,function(err){
//      if(err)console.log(err);
//      else console.log(dishNew.toResponse());
//    });
//  });
//
//
// });*/
//
// /*var ingredient = new Ingredient({id:2});
// ingredient.delete(function(err){
//   if(err)console.log(err);
//   else console.log("deleted");
// });*/
//
// // var user = new User({id:27});
// // user.delete(function(err){
// //   if(err)console.log(err);
// //   else console.log("user deleted succesfully");
// // });
