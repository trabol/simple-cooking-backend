let jwt=require("jsonwebtoken");
let authConfig=require("../config/auth");
let Error = require("./Error")
class JWTPromise{
  cosntruct(){

  }
  async sign(data){
    return await new Promise((resolve,reject)=>{
      jwt.sign(data, authConfig.jwtSecret, function(err, token) {
        if(err)reject(err);
        else resolve(token);
      });
    }).catch(err=>{throw err});
  }
  async verify(token){
    return await new Promise((resolve,reject)=>{
      jwt.verify(token,authConfig.jwtSecret,function(err,decoded){
        if(err){
          reject(Error.Errors.INVALID_ACCESS_TOKEN);
        }else{
          resolve(decoded);
        }
      });
    }).catch(err=>{throw err});
  }
}
var jwtObj = new JWTPromise();
module.exports=jwtObj;
