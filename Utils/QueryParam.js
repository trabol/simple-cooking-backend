

var Locations={
  BODY:"body",
  QUERY:"query",
}

var QueryParam=function(name,location){
  this.name=name||"";
  this.location=location||Locations.QUERY;
}
QueryParam.Locations=Locations;

module.exports=QueryParam;
