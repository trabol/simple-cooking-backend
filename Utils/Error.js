

var Code={
  GENERAL:-1,
  NOT_FOUND:1,
  DB_ERROR:2,
  INVALID_ACCESS_TOKEN:3,
  NOT_YOUR_DISH:4,
  INGREDIENT_ALLREADY_EXISTS:5,
  INGREDIENT_DOESNT_EXIST:6,
  DISH_DOESNT_EXIST:7,
  USER_DOESNT_EXIST:8,
  WRONG_REFRESH_TOKEN:9,
  NO_ACCESS_TOKEN:10,
  NOT_ENOUGH_DATA:11,
  USER_HAS_NO_DISHES:12,
  DB_CONNECTION_ERROR:13,
  GOOGLE_AUTH_ERROR:14,
  NO_DISH_WITH_INGREDIENTS:15,
  WRONG_FILE_FORMAT:1000
}

var Status={
  UNAUTHORIZED:401,
  FORBIDDEN:403,
  NOT_FOUND:404,
  CONFLICT:409,
  INTERNAL:500,
  UNPROCESSABLE_ENTITY:422,
  NO_CONTENT:204


}

var Message={
  DB_ERROR:"Databse Error",
  NOT_YOUR_DISH:"It's not your Dish",
  INGREDIENT_ALLREADY_EXISTS:"This Ingredient Alread exists",
  INGREDIENT_DOESNT_EXIST:"this ingredient doesn't exist",
  DISH_DOESNT_EXIST:"This dish doesn't exist",
  USER_DOESNT_EXIST:"This user doesn't exist",
  INVALID_ACCESS_TOKEN:"Invalid access token",
  WRONG_REFRESH_TOKEN:"Wrong refresh token",
  NO_ACCESS_TOKEN:"No access_token provided",
  NOT_ENOUGH_DATA:"not enough data",
  NOT_FOUND:"This resource doesn't exist",
  INTERNAL:"Internal server Error",
  USER_HAS_NO_DISHES:"This user doesn't have any dishes",
  DB_CONNECTION_ERROR:"Couldn't connect to a database",
  GOOGLE_AUTH_ERROR:"We have problems with authenticating you with Google Plus services",
  NO_DISH_WITH_INGREDIENTS:"There are no dishes using only those ingredients",
  WRONG_FILE_FORMAT:"File format is wrong, only .png files are allowed"
}

var Error=function(code,status,message){
  this.status=status||Status.INTERNAL;
  this.code=code||Code.GENERAL;
  this.message=message||"Server Error";
};

var Errors={
  NOT_FOUND:new Error(Code.NOT_FOUND,Status.NOT_FOUND,Message.NOT_FOUND),
  DB_ERROR:new Error(Code.DB_ERROR,Status.INTERNAL,Message.DB_ERROR),
  DB_CONNECTION_ERROR:new Error(Code.DB_CONNECTION_ERROR,Status.INTERNAL,Message.DB_CONNECTION_ERROR),
  NOT_YOUR_DISH:new Error(Code.NOT_YOUR_DISH,Status.FORBIDDEN,Message.NOT_YOUR_DISH),
  INGREDIENT_ALLREADY_EXISTS:new Error(Code.INGREDIENT_ALLREADY_EXISTS,Status.CONFLICT,Message.INGREDIENT_ALLREADY_EXISTS),
  INGREDIENT_DOESNT_EXIST:new Error(Code.INGREDIENT_DOESNT_EXIST,Status.NOT_FOUND,Message.INGREDIENT_DOESNT_EXIST),
  DISH_DOESNT_EXIST:new Error(Code.DISH_DOESNT_EXIST,Status.NOT_FOUND,Message.DISH_DOESNT_EXIST),
  USER_DOESNT_EXIST:new Error(Code.USER_DOESNT_EXIST,Status.NOT_FOUND,Message.USER_DOESNT_EXIST),
  INVALID_ACCESS_TOKEN:new Error(Code.INVALID_ACCESS_TOKEN,Status.UNAUTHORIZED,Message.INVALID_ACCESS_TOKEN),
  WRONG_REFRESH_TOKEN:new Error(Code.WRONG_REFRESH_TOKEN,Status.FORBIDDEN,Message.WRONG_REFRESH_TOKEN),
  NO_ACCESS_TOKEN:new Error(Code.NO_ACCESS_TOKEN,Status.UNAUTHORIZED,Message.NO_ACCESS_TOKEN),
  NOT_ENOUGH_DATA:new Error(Code.NOT_ENOUGH_DATA,Status.UNPROCESSABLE_ENTITY,Message.NOT_ENOUGH_DATA),
  INTERNAL:new Error(Code.GENERAL,Status.INTERNAL,Message.INTERNAL),
  USER_HAS_NO_DISHES:new Error(Code.USER_HAS_NO_DISHES,Status.NO_CONTENT,Message.USER_HAS_NO_DISHES),
  GOOGLE_AUTH_ERROR:new Error(Code.GOOGLE_AUTH_ERROR,Status.UNAUTHORIZED,Message.GOOGLE_AUTH_ERROR),
  NO_DISH_WITH_INGREDIENTS:new Error(Code.NO_DISH_WITH_INGREDIENTS,Status.NOT_FOUND,Message.NO_DISH_WITH_INGREDIENTS),
  WRONG_FILE_FORMAT:new Error(Code.WRONG_FILE_FORMAT,Status.UNPROCESSABLE_ENTITY,Message.WRONG_FILE_FORMAT)
}



Error.Code=Code;
Error.Status=Status;
Error.Message=Message;
Error.Errors=Errors;

module.exports=Error;
